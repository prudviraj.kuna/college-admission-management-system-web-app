package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.Admission_Details;
import model.Admission_Portal;

import util.Database;
import util.Query;


public class AdmissPort_Impl implements Admiss_PortDAO {
	PreparedStatement pst;
	ResultSet rs;
	int result;

	@Override
	public int AdmisPortalCheck(Admission_Portal portal) {
		result = 0;
		try {
			pst = Database.getConnection().prepareStatement(Query.admissionlog);
			pst.setString(1, portal.getUid());
			pst.setString(2, portal.getPassword());
			rs = pst.executeQuery();
			while (rs.next()) {
				result++;
			}
		} catch (ClassNotFoundException | SQLException e) {

			System.out.println("Getting exception on AdmissionPortal login");

		} finally {

			try {
				pst.close();
				rs.close();
				Database.getConnection().close();
			} catch (ClassNotFoundException | SQLException e) {

			}
		}
		

		return result;
	}

	
		@Override
		public List<Admission_Details> viewAdmissionDetails(){
			List<Admission_Details> list=new ArrayList<Admission_Details>();
			try {
				pst=Database.getConnection().prepareStatement(Query.viewAll);
				rs=pst.executeQuery();
				while(rs.next()) {
					Admission_Details det=new Admission_Details(rs.getString(1),rs.getInt(2),rs.getString(3),rs.getInt(4),rs.getLong("mobilenum"));
					list.add(det);
				}
			} catch (ClassNotFoundException |SQLException e) {
				// TODO Auto-generated catch block
				System.out.println("Exception occurs in Admission Details");
			} finally {
				try {
					Database.getConnection().close();
					rs.close();
					pst.close();
				}catch(ClassNotFoundException |SQLException e) {
					
				}
			}
			return list;
		

	
	}


		@Override
		public void removeStud(Admission_Details detail) {
			try {
				pst=Database.getConnection().prepareStatement(Query.removeStud);
			    pst.setInt(1, detail.getDeptid());
			    pst.executeUpdate();
			}catch(SQLException|ClassNotFoundException e){
				
			}
			
		}


		@Override
		public void update(Admission_Details detail) {
			try {
				pst=Database.getConnection().prepareStatement(Query.update);
			    pst.setString(1, detail.getStudname());
			    pst.setInt(2,detail.getDeptid());
			    pst.executeUpdate();
			}catch(SQLException|ClassNotFoundException e){
				
			}
			
		}


		@Override
		public int addstud(Admission_Details detail) {
			result=0;
			try {
				pst=Database.getConnection().prepareStatement(Query.addstud);
			    pst.setString(1, detail.getStudname());
			    pst.setInt(2, detail.getAdmissionid());
			    pst.setString(3, detail.getDeptname());
			    pst.setInt(4,detail.getDeptid());
			    pst.setLong(5, detail.getMobilenum());
			    result=pst.executeUpdate();
			}catch(SQLException|ClassNotFoundException e){
				return result;
			}
			return result;
		}
}

	