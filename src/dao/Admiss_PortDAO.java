package dao;
import java.util.List;

import model.Admission_Details;

import model.Admission_Portal;

public interface Admiss_PortDAO {
  
  public int AdmisPortalCheck(Admission_Portal portal);
  public List<Admission_Details> viewAdmissionDetails();
  public void removeStud(Admission_Details detail);
  public void update(Admission_Details detail);
  public int addstud(Admission_Details detail);
}