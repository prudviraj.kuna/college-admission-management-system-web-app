package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.AdmissPort_Impl;
import dao.Admiss_PortDAO;
import model.Admission_Details;
import model.Admission_Portal;


@WebServlet("/creden")
public class AdmissionController extends HttpServlet {
	private static final long serialVersionUID = 1L;


	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String uid=request.getParameter("uid");
		String password=request.getParameter("password");
		Admission_Portal portal=new Admission_Portal(uid,password);
		Admiss_PortDAO dao=new AdmissPort_Impl();
		int result=dao.AdmisPortalCheck(portal);
		if(result>0) {
			List<Admission_Details> list=dao.viewAdmissionDetails();
			request.setAttribute("admission",list);
			request.getRequestDispatcher("admission.jsp").forward(request, response);
		}else {
			request.setAttribute("error","Hello "+ uid +", Please check your portal credentials..");
			request.getRequestDispatcher("index.jsp").forward(request, response);
		}
	}

}
