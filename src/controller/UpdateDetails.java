package controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.AdmissPort_Impl;
import dao.Admiss_PortDAO;
import model.Admission_Details;


@WebServlet("/updateCont")
public class UpdateDetails extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws
	ServletException, IOException {
		String studname=request.getParameter("studname");
		int deptid=Integer.parseInt(request.getParameter("deptid"));
		Admission_Details detail=new Admission_Details();
		detail.setStudname(studname);
		detail.setDeptid(deptid);
		Admiss_PortDAO dao=new AdmissPort_Impl();
		dao.update(detail);
		request.getRequestDispatcher("after").include(request, response);
	}

}
