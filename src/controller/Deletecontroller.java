package controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.AdmissPort_Impl;
import dao.Admiss_PortDAO;
import model.Admission_Details;


@WebServlet("/delete")
public class Deletecontroller extends HttpServlet {
	private static final long serialVersionUID = 1L;
      
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		int deptid=Integer.parseInt(request.getParameter("deptid"));
		Admiss_PortDAO dao=new AdmissPort_Impl();
		Admission_Details detail=new Admission_Details();
		detail.setDeptid(deptid);
		dao.removeStud(detail);		
		PrintWriter out=response.getWriter();
		response.setContentType("text/html");
		out.print(deptid+ " deleted successfully<br/>");
		request.getRequestDispatcher("admissioncrud").include(request,response);
	}

}
