package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.AdmissPort_Impl;
import dao.Admiss_PortDAO;
import model.Admission_Details;

@WebServlet("/add")
public class AddController extends HttpServlet {
	private static final long serialVersionUID = 1L;
   
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String studname=request.getParameter("studname");
		int admissionid=Integer.parseInt(request.getParameter("admissionid"));
		String deptname=request.getParameter("deptname");
		Integer deptid=Integer.parseInt(request.getParameter("deptid"));
		long mobilenum=Long.parseLong(request.getParameter("mobilenum"));
		Admission_Details detail=new Admission_Details(studname,admissionid,deptname,deptid,mobilenum);
		Admiss_PortDAO dao=new AdmissPort_Impl();
		int result=dao.addstud(detail);
		if(result>0) {
			request.setAttribute("msg", admissionid+" admission id, with details inserted successfully!!!<br/>");
		}else {
			request.setAttribute("msg", admissionid+" Duplicate Data");
		}
		List<Admission_Details> list=dao.viewAdmissionDetails();
		request.setAttribute("admission",list);
		request.getRequestDispatcher("admissioncrud.jsp").include(request, response);
	}

}
