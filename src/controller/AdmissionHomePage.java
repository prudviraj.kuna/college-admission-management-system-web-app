package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.AdmissPort_Impl;
import dao.Admiss_PortDAO;
import model.Admission_Details;


@WebServlet("/home")
public class AdmissionHomePage extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Admiss_PortDAO dao=new AdmissPort_Impl();
		List<Admission_Details> list=dao.viewAdmissionDetails();
		request.setAttribute("admission",list);
		request.getRequestDispatcher("admission.jsp").forward(request, response);
	}

}
 