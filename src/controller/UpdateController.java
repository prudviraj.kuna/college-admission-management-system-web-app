package controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Admission_Details;



@WebServlet("/update")
public class UpdateController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
  
	protected void doGet(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException {
		String studname=request.getParameter("studname");
		int deptid=Integer.parseInt(request.getParameter("deptid"));
		
		Admission_Details detail=new Admission_Details();
		detail.setStudname(studname);
		detail.setDeptid(deptid);
		request.setAttribute("admission", detail);
		request.getRequestDispatcher("update.jsp").forward(request, response);;
		
	}

}
