<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<style>
body {
	background-color: #f2f8fa;
}

table {
	text-align: center;
}

th {
	color: #db5a6b;
}
</style>
<meta charset="UTF-8">
<title>Admission CRUD</title>

</head>
<body>
	<h1>Admission CRUD Operations Page</h1>
	<hr />
	<p align="right">
		<a href="home">Home</a> <a href="insert">Insert Students</a> <a
			href="logout">Logout</a>
	</p>
	<hr />
	${msg}
	<table border="6">
		<tr>
			<th colspan="7">Admission Details</th>
		</tr>
		<tr>
			<th>Student Name</th>
			<th>Admission ID</th>
			<th>Department Name</th>
			<th>Department ID</th>
			<th>Mobile number</th>
			<th>Update</th>
			<th>Delete</th>
		</tr>
		<c:forEach items="${admission}" var="e">
			<tr>
				<td>${e.getStudname()}</td>
				<td>${e.getAdmissionid()}</td>
				<td>${e.getDeptname()}</td>
				<td>${e.getDeptid()}</td>
				<td>${e.getMobilenum()}</td>
				<td><a style="text-decoration: none"
					href="update?deptid=${e.getDeptid()}&studname=${e.getStudname()}">Update</a></td>
				<td><a style="text-decoration: none"
					href="delete?deptid=${e.getDeptid()}">Delete</a></td>
			</tr>
		</c:forEach>
	</table>
</body>
</html>