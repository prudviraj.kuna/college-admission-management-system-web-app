<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<style>
body{
background-color:#f2f8fa;
}
table{
text-align:center;
}
th{
color:#db5a6b;
}
</style>
<meta charset="UTF-8">
<title>Admission</title>

</head>
<body>
	<h1>Admission Portal Page</h1>
	<hr />
	<p align="right">
	<a href="admissioncrud" style="text-decoration:none">Admission CRUD</a>
		<a href="logout">Logout</a>
	</p>
	<hr />
	<table border="6">
		<tr>
			<th colspan="5">Admission Details</th>
		</tr>
		<tr>
			<th>Student Name</th>
			<th>Admission ID</th>
			<th>Department Name</th>
			<th>Department ID</th>
			<th>Mobile number</th>
			
		</tr>
		<c:forEach items="${admission}" var="e">
			<tr>
				<td>${e.getStudname()}</td>
				<td>${e.getAdmissionid()}</td>
				<td>${e.getDeptname()}</td>
				<td>${e.getDeptid()}</td>
				<td>${e.getMobilenum()}</td>
				
			</tr>
		</c:forEach>
	</table>
</body>
</html>